import sqlite3

conn = sqlite3.connect('student.db')
conn.row_factory = sqlite3.Row # return rows as dict
cur = conn.cursor()
cur.execute("""
CREATE TABLE IF NOT EXISTS solo (
    participant_id INTEGER,
    name varchar(50) NOT NULL,
    email varchar(50) NOT NULL,
    phone varchar(13) NOT NULL UNIQUE,
    college varchar(50) NOT NULL,
    paid tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (participant_id));
""")

conn.commit()

def all_students():
    cur.execute("SELECT * FROM solo")
    col_names = [c[0] for c in cur.description]
    return list(map(dict, cur.fetchall())), col_names

def mark_paid(pid):
    cur.execute("update solo set paid = true where participant_id = ?", (pid,))
    updated = cur.rowcount != 0
    conn.commit()
    return updated

def find_student(mobile):
    cur.execute("SELECT participant_id, name, phone, college FROM solo WHERE paid AND phone = ?", (mobile,))
    rec = cur.fetchone()
    if rec is not None:
        rec = dict(rec)
    return rec

def add_student(name, phone, email, college):
    try:
        cur.execute('insert into solo(name, phone, email, college) values (?, ?, ?, ?)', 
                    (name, phone, email, college))
        added = cur.rowcount == 1
        if added:
            conn.commit()
        return added
    except: # not added
        return False
