#!/usr/bin/env python3
from sanic import Sanic
from sanic.response import json, redirect
import db

app = Sanic()

@app.route('/')
def index(request):
    return redirect('/static/register.html')

@app.route("/solo-reg", methods=['POST'])
def solo_reg(request):
    stu = { attr: v for (attr, [v]) in request.form.items() }
    added = db.add_student(
        name=stu['name'],
        email=stu['email'],
        phone=stu['contact'],
        college=stu['college']
    )
    return json({'status': added})

@app.route("/solo-regs")
def solo_regs(request):
    students, cols = db.all_students()
    return json({'header':cols, 'students': students})

@app.route("/solo-pay", methods=['POST'])
def mark_payment(request):
    resp = {}
    P = request.json
    pw = P['password']
    pids = P['payer_ids']

    if pw != "bonedance":
        resp['status'] = 'wrong password'
    else:
        status = True
        for pid in pids:
            status = status and db.mark_paid(pid)
        resp['status'] = "Paid" if status else "Payment Failed"
    return json(resp)

@app.route("/id-card", methods=['POST'])
def id_card(request):
    resp = {}
    P = request.json
    mobile = P['mobile']
    student = db.find_student(mobile)

    if student is None:
        resp['status'] = "fail"
        resp['error'] = "Number not found among paid"
    else:
        s = student
        pre = "SO"
        pid = s["participant_id"]

        resp['status'] = "yey!";
        resp['student'] = {
            "participant-id": "{pre}-{pid}".format(pre=pre, pid=pid),
            "student-name": s["name"],
            "college": s["college"],
            "mobile": s["phone"],
        }
    return json(resp)

if __name__ == "__main__":
    app.static('/static', './static/')
    app.static('/p', './static/p/')
    app.run(host="0.0.0.0", port=8000)
